#ifndef IOTECH__H_
#define IOTECH__H_

#include "stm32f1xx_hal.h"

#define base_address 0x8007800

uint16_t iotech_read_node_address(void);
uint16_t iotech_read_group_address(uint8_t num);
uint16_t iotech_get_groups_size(void);
void iotech_set_groups_size(uint8_t size);
void iotech_write_node_address(uint16_t addr);
void iotech_add_group(uint16_t addr);
void iotech_remove_group(uint16_t addr);

#endif
