#ifndef IOTECH_LORA_H_
#define IOTECH_LORA_H_

#include "stm32f1xx_hal.h"
#include "gpio.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "gpio.h"
#include "spi.h"
#include "iotech_sx127x.h"

#define packet_size 8



/* Type Defenitions */
typedef struct {
	uint8_t hub_id;
	uint8_t node_id;
	uint8_t dimmer[2];
} node_data_t;

extern iotech_sx127x sx;
/* Functions */
void iotech_lora_init(uint8_t channel, uint8_t power);
void iotech_lora_send_data_no_ack(node_data_t *data);
void iotech_lora_wait_to_send(void);

//amir hossein rassafi

#endif
