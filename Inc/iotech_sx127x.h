#ifndef __BARBOD_RFM9X_H__
#define __BARBOD_RFM9X_H__

#include "stm32f1xx_hal.h"

	
#define iotech_sx127x_SPI_WRITE_MASK 0x80
//not all bit of iotech_sx127x_SPI_WRITE_MASK
#define iotech_sx127x_SPI_READ_MASK 0x7f

// The crystal oscillator frequency of the module
#define iotech_sx127x_FXOSC 32000000.0

#define MAX_PKT_LENGTH           255

// Register names (LoRa Mode, from table 85)
#define iotech_sx127x_REG_00_FIFO                                0x00
#define iotech_sx127x_REG_01_OP_MODE                             0x01
#define iotech_sx127x_REG_02_RESERVED                            0x02
#define iotech_sx127x_REG_03_RESERVED                            0x03
#define iotech_sx127x_REG_04_RESERVED                            0x04
#define iotech_sx127x_REG_05_RESERVED                            0x05
#define iotech_sx127x_REG_06_FRF_MSB                             0x06
#define iotech_sx127x_REG_07_FRF_MID                             0x07
#define iotech_sx127x_REG_08_FRF_LSB                             0x08
#define iotech_sx127x_REG_09_PA_CONFIG                           0x09
#define iotech_sx127x_REG_0A_PA_RAMP                             0x0a
#define iotech_sx127x_REG_0B_OCP                                 0x0b
#define iotech_sx127x_REG_0C_LNA                                 0x0c
#define iotech_sx127x_REG_0D_FIFO_ADDR_PTR                       0x0d
#define iotech_sx127x_REG_0E_FIFO_TX_BASE_ADDR                   0x0e
#define iotech_sx127x_REG_0F_FIFO_RX_BASE_ADDR                   0x0f
#define iotech_sx127x_REG_10_FIFO_RX_CURRENT_ADDR                0x10
#define iotech_sx127x_REG_11_IRQ_FLAGS_MASK                      0x11
#define iotech_sx127x_REG_12_IRQ_FLAGS                           0x12
#define iotech_sx127x_REG_13_RX_NB_BYTES                         0x13
#define iotech_sx127x_REG_14_RX_HEADER_CNT_VALUE_MSB             0x14
#define iotech_sx127x_REG_15_RX_HEADER_CNT_VALUE_LSB             0x15
#define iotech_sx127x_REG_16_RX_PACKET_CNT_VALUE_MSB             0x16
#define iotech_sx127x_REG_17_RX_PACKET_CNT_VALUE_LSB             0x17
#define iotech_sx127x_REG_18_MODEM_STAT                          0x18
#define iotech_sx127x_REG_19_PKT_SNR_VALUE                       0x19
#define iotech_sx127x_REG_1A_PKT_RSSI_VALUE                      0x1a
#define iotech_sx127x_REG_1B_RSSI_VALUE                          0x1b
#define iotech_sx127x_REG_1C_HOP_CHANNEL                         0x1c
#define iotech_sx127x_REG_1D_MODEM_CONFIG1                       0x1d
#define iotech_sx127x_REG_1E_MODEM_CONFIG2                       0x1e
#define iotech_sx127x_REG_1F_SYMB_TIMEOUT_LSB                    0x1f
#define iotech_sx127x_REG_20_PREAMBLE_MSB                        0x20
#define iotech_sx127x_REG_21_PREAMBLE_LSB                        0x21
#define iotech_sx127x_REG_22_PAYLOAD_LENGTH                      0x22
#define iotech_sx127x_REG_23_MAX_PAYLOAD_LENGTH                  0x23
#define iotech_sx127x_REG_24_HOP_PERIOD                          0x24
#define iotech_sx127x_REG_25_FIFO_RX_BYTE_ADDR                   0x25
#define iotech_sx127x_REG_26_MODEM_CONFIG3                       0x26

#define iotech_sx127x_REG_40_DIO_MAPPING1                        0x40
#define iotech_sx127x_REG_41_DIO_MAPPING2                        0x41
#define iotech_sx127x_REG_42_VERSION                             0x42

#define iotech_sx127x_REG_4B_TCXO                                0x4b
#define iotech_sx127x_REG_4D_PA_DAC                              0x4d
#define iotech_sx127x_REG_5B_FORMER_TEMP                         0x5b
#define iotech_sx127x_REG_61_AGC_REF                             0x61
#define iotech_sx127x_REG_62_AGC_THRESH1                         0x62
#define iotech_sx127x_REG_63_AGC_THRESH2                         0x63
#define iotech_sx127x_REG_64_AGC_THRESH3                         0x64

// iotech_sx127x_REG_01_OP_MODE                             0x01
#define iotech_sx127x_LONG_RANGE_MODE                       0x80
#define iotech_sx127x_ACCESS_SHARED_REG                     0x40
#define iotech_sx127x_MODE                                  0x07
#define iotech_sx127x_MODE_SLEEP                            0x00
#define iotech_sx127x_MODE_STDBY                            0x01
#define iotech_sx127x_MODE_FSTX                             0x02
#define iotech_sx127x_MODE_TX                               0x03
#define iotech_sx127x_MODE_FSRX                             0x04
#define iotech_sx127x_MODE_RXCONTINUOUS                     0x05
#define iotech_sx127x_MODE_RXSINGLE                         0x06
#define iotech_sx127x_MODE_CAD                              0x07

// iotech_sx127x_REG_09_PA_CONFIG                           0x09	checked
#define iotech_sx127x_PA_BOOST                              0x80
#define iotech_sx127x_MAX_POWER                             0x70
#define iotech_sx127x_OUTPUT_POWER                          0x0f

// iotech_sx127x_REG_0A_PA_RAMP                             0x0a
#define iotech_sx127x_LOW_PN_TX_PLL_OFF                     0x10
#define iotech_sx127x_PA_RAMP                               0x0f
#define iotech_sx127x_PA_RAMP_3_4MS                         0x00
#define iotech_sx127x_PA_RAMP_2MS                           0x01
#define iotech_sx127x_PA_RAMP_1MS                           0x02
#define iotech_sx127x_PA_RAMP_500US                         0x03
#define iotech_sx127x_PA_RAMP_250US                         0x0
#define iotech_sx127x_PA_RAMP_125US                         0x05
#define iotech_sx127x_PA_RAMP_100US                         0x06
#define iotech_sx127x_PA_RAMP_62US                          0x07
#define iotech_sx127x_PA_RAMP_50US                          0x08
#define iotech_sx127x_PA_RAMP_40US                          0x09
#define iotech_sx127x_PA_RAMP_31US                          0x0a
#define iotech_sx127x_PA_RAMP_25US                          0x0b
#define iotech_sx127x_PA_RAMP_20US                          0x0c
#define iotech_sx127x_PA_RAMP_15US                          0x0d
#define iotech_sx127x_PA_RAMP_12US                          0x0e
#define iotech_sx127x_PA_RAMP_10US                          0x0f

// iotech_sx127x_REG_0B_OCP                                 0x0b
#define iotech_sx127x_OCP_MAX                               0x3f

// iotech_sx127x_REG_0C_LNA                                 0x0c
#define iotech_sx127x_LNA_GAIN                              0xe0
#define iotech_sx127x_LNA_BOOST                             0x03
#define iotech_sx127x_LNA_BOOST_DEFAULT                     0x00
#define iotech_sx127x_LNA_BOOST_150PC                       0x11 //highest gain

// iotech_sx127x_REG_11_IRQ_FLAGS_MASK                      0x11 
#define iotech_sx127x_RX_TIMEOUT_MASK                       0x80
#define iotech_sx127x_RX_DONE_MASK                          0x40
#define iotech_sx127x_PAYLOAD_CRC_ERROR_MASK                0x20
#define iotech_sx127x_VALID_HEADER_MASK                     0x10
#define iotech_sx127x_TX_DONE_MASK                          0x08
#define iotech_sx127x_CAD_DONE_MASK                         0x04
#define iotech_sx127x_FHSS_CHANGE_CHANNEL_MASK              0x02
#define iotech_sx127x_CAD_DETECTED_MASK                     0x01

// iotech_sx127x_REG_12_IRQ_FLAGS                           0x12 
#define iotech_sx127x_RX_TIMEOUT                            0x80
#define iotech_sx127x_RX_DONE                               0x40
#define iotech_sx127x_PAYLOAD_CRC_ERROR                     0x20
#define iotech_sx127x_VALID_HEADER                          0x10
#define iotech_sx127x_TX_DONE                               0x08
#define iotech_sx127x_CAD_DONE                              0x04
#define iotech_sx127x_FHSS_CHANGE_CHANNEL                   0x02
#define iotech_sx127x_CAD_DETECTED                          0x01

// iotech_sx127x_REG_18_MODEM_STAT                          0x18			
#define iotech_sx127x_RX_CODING_RATE                        0xe0
#define iotech_sx127x_MODEM_STATUS_CLEAR                    0x10
#define iotech_sx127x_MODEM_STATUS_HEADER_INFO_VALID        0x08
#define iotech_sx127x_MODEM_STATUS_RX_ONGOING               0x04
#define iotech_sx127x_MODEM_STATUS_SIGNAL_SYNCHRONIZED      0x02
#define iotech_sx127x_MODEM_STATUS_SIGNAL_DETECTED          0x01

// iotech_sx127x_REG_1C_HOP_CHANNEL                         0x1c
#define iotech_sx127x_PLL_TIMEOUT                           0x80
#define iotech_sx127x_RX_PAYLOAD_CRC_IS_ON                  0x40
#define iotech_sx127x_FHSS_PRESENT_CHANNEL                  0x3f

// iotech_sx127x_REG_1D_MODEM_CONFIG1                       0x1d checked
#define iotech_sx127x_CODING_RATE_4_5                       0x02
#define iotech_sx127x_CODING_RATE_4_6                       0x04
#define iotech_sx127x_CODING_RATE_4_7                       0x06
#define iotech_sx127x_CODING_RATE_4_8                       0x08
#define iotech_sx127x_IMPLICIT_HEADER_MODE_ON               0x01
#define iotech_sx127x_IMPLICIT_HEADER_MODE_OFF              0xfe



// iotech_sx127x_REG_1E_MODEM_CONFIG2                       0x1e checked
#define iotech_sx127x_TX_CONTINUOUS_MODE                    0x08
#define iotech_sx127x_CRC_AUTO_ON                           0x04
#define iotech_sx127x_SYM_TIMEOUT_MSB                       0x03

// iotech_sx127x_REG_4D_PA_DAC                              0x4d
#define iotech_sx127x_PA_DAC_DISABLE                        0x84
#define iotech_sx127x_PA_DAC_ENABLE                         0x87




typedef struct iotech_sx127x{
	GPIO_TypeDef* 	NSS_PORT;
	uint16_t				NSS_PIN;
	GPIO_TypeDef* 	RESET_PORT;
	uint16_t				RESET_PIN;
	SPI_HandleTypeDef *hspi;
	//need to add interupt
}iotech_sx127x;
	

void iotech_sx127x_sleep(iotech_sx127x *rfm);
void iotech_sx127x_idle(iotech_sx127x *rfm);
void iotech_sx127x_implicitHeaderMode(iotech_sx127x *rfm);
void iotech_sx127x_explicitHeaderMode(iotech_sx127x *rfm);
void iotech_sx127x_setTxPower(iotech_sx127x *rfm,int level);
void iotech_sx127x_setFrequency(iotech_sx127x *rfm,uint64_t frequency);
void iotech_sx127x_setSpreadingFactor(iotech_sx127x *rfm,int sf);
void iotech_sx127x_setSignalBandwidth(iotech_sx127x *rfm,long sbw);
void iotech_sx127x_setCodingRate4(iotech_sx127x *rfm,int denominator);
void iotech_sx127x_setPreambleLength(iotech_sx127x *rfm,long length);
void iotech_sx127x_crc(iotech_sx127x *rfm);
void iotech_sx127x_noCrc(iotech_sx127x *rfm);
uint8_t iotech_sx127x_begin(iotech_sx127x *rfm,uint8_t channel,uint8_t power);
void iotech_sx127x_end(iotech_sx127x *rfm);
uint8_t iotech_sx127x_beginPacket(iotech_sx127x *rfm,uint8_t implicitHeader);
uint8_t iotech_sx127x_endPacket(iotech_sx127x *rfm);
uint8_t iotech_sx127x_parsePacket(iotech_sx127x *rfm,uint8_t size);
int8_t iotech_sx127x_packetRssi(iotech_sx127x *rfm);
//float packetSnr();
uint8_t iotech_sx127x_write(iotech_sx127x *rfm,const uint8_t *buffer,uint8_t size);
void iotech_sx127x_receive(iotech_sx127x *rfm,uint8_t size);
uint8_t iotech_sx127x_available(iotech_sx127x *rfm);
uint8_t iotech_sx127x_read(iotech_sx127x *rfm);
uint8_t iotech_sx127x_peek(iotech_sx127x *rfm);
void iotech_sx127x_flush(iotech_sx127x *rfm);
void iotech_sx127x_send(iotech_sx127x *rfm,uint8_t * data,uint8_t size, uint8_t retry);

uint8_t iotech_sx127x_readregister(iotech_sx127x *rfm,uint8_t name);
void iotech_sx127x_read_wait_to_send(iotech_sx127x *rfm);

#endif
