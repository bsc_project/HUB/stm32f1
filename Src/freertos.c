/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "gpio.h"
#include "spi.h"
#include "iotech_sx127x.h"
#include "iotech_lora.h"
#include "iotech_flash.h"
#include "usart.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;
osThreadId LoraRxTaskHandle;
osThreadId LoraTxTaskHandle;
osThreadId ESPHandle;
osMessageQId uart_bufferHandle;

/* USER CODE BEGIN Variables */
#define number_of_node 8
uint8_t buff[20], i, size;
uint8_t tst_buff[5] = {1, 2, 3, 4, 5};
uint8_t packetsize = 0;
uint8_t a_data[2] = {55, 85};
uint8_t uart_buf[100];
node_data_t nodes[10];
SemaphoreHandle_t lora_semaphore;
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);
void StartLoraRxTask(void const * argument);
void StartLoraTxTask(void const * argument);
void esp82(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

	iotech_lora_init(10, 20);						   /* init sx127x LORA module */
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of RxBinary */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
   lora_semaphore = xSemaphoreCreateMutex();

  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of LoraRxTask */
  osThreadDef(LoraRxTask, StartLoraRxTask, osPriorityIdle, 0, 128);
  LoraRxTaskHandle = osThreadCreate(osThread(LoraRxTask), NULL);

  /* definition and creation of LoraTxTask */
  osThreadDef(LoraTxTask, StartLoraTxTask, osPriorityIdle, 0, 128);
  LoraTxTaskHandle = osThreadCreate(osThread(LoraTxTask), NULL);

  /* definition and creation of ESP */
  osThreadDef(ESP, esp82, osPriorityIdle, 0, 128);
  ESPHandle = osThreadCreate(osThread(ESP), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of uart_buffer */
/* what about the sizeof here??? cd native code */
  osMessageQDef(uart_buffer, 50, uint8_t);
  uart_bufferHandle = osMessageCreate(osMessageQ(uart_buffer), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
	//not handled any thing
  for(;;)
  {
		
    osDelay(1);
		
  }
  /* USER CODE END StartDefaultTask */
}

/* StartLoraRxTask function */
void StartLoraRxTask(void const * argument)
{
  /* USER CODE BEGIN StartLoraRxTask */
  /* Infinite loop */
	//now we do not need to recieve any thing from nodes
	uint32_t last_time_led_state_changed = 0;
	last_time_led_state_changed = HAL_GetTick();
  for(;;)
  {
    osDelay(1);
		if(HAL_GetTick() - last_time_led_state_changed > 400)
		{
			HAL_GPIO_TogglePin(LED4_GPIO_Port, LED4_Pin);
			last_time_led_state_changed = HAL_GetTick();
		}

  }
  /* USER CODE END StartLoraRxTask */
}

/* StartLoraTxTask function */
void StartLoraTxTask(void const * argument)
{
  /* USER CODE BEGIN StartLoraTxTask */
  /* Infinite loop */
	//this task is for scheduling nodes
	uint32_t last_pressed_time = 0;
	uint8_t id = 0;
  for(;;)
  {
		osDelay(1);
		if(HAL_GPIO_ReadPin(Button_GPIO_Port, Button_Pin) == GPIO_PIN_RESET)
		{
				if(HAL_GetTick() - last_pressed_time > 1000)
				{
					last_pressed_time = HAL_GetTick();
					HAL_GPIO_WritePin(ESP_RST_GPIO_Port, ESP_RST_Pin, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
					osDelay(50);
			    HAL_GPIO_WritePin(ESP_RST_GPIO_Port, ESP_RST_Pin, GPIO_PIN_SET);
					HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
				}
		}
		else
		{
			last_pressed_time = HAL_GetTick();
		}
		xSemaphoreTake(lora_semaphore, 100);
		iotech_lora_send_data_no_ack(&nodes[id]);
		iotech_lora_wait_to_send();
		id++;
		xSemaphoreGive(lora_semaphore);
		
		if(id == number_of_node)
		 id = 0;
  }
  /* USER CODE END StartLoraTxTask */
}

/* esp82 function */
void esp82(void const * argument)
{
  /* USER CODE BEGIN esp82 */

	
  /* Infinite loop */

	HAL_UART_Receive_DMA(&huart2, uart_buf, sizeof(uart_buf)); 
__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
	uint8_t new_data,data,packet_cnt,Check_Sum,packet_is_valid=0;
	uint8_t ID,Len,byte[50],byte_cnt;
  for(;;)
  {
    osDelay(1);
			new_data = 0;
		//layer 0 => found 0xff 0xff id len ... data ... check_sum packet 
		if(!xQueueIsQueueEmptyFromISR(uart_bufferHandle))
			{
				xQueueReceive(uart_bufferHandle,&data,0);
				new_data = 1;	
			}
			if(new_data)
			{
				switch(packet_cnt){
					case 0:
							Check_Sum = 0;
							if (data == 0xff)
								packet_cnt++;
							else
								packet_cnt = 0;
						break;
					case 1:
							if (data == 0xff)
								packet_cnt++;
							else
								packet_cnt=0;
						break;
					case 2:
							ID = data;
							Check_Sum += ID;
							packet_cnt++;
						break;
					case 3:
							Len = data;
							Check_Sum += Len;
							packet_cnt++;
						break;
					case 4:
							byte[byte_cnt++] = data;
							if(Len>1)
								Check_Sum += data;
							if(Len == 1)
							{								
								if(Check_Sum+byte[byte_cnt-1]==0xff)
								{
									packet_is_valid = 1;							
								}
								byte_cnt = 0;
								packet_cnt = 0;
							}
							else
								Len--;	
						break;
				}
			}
			if(packet_is_valid)
			{
				xSemaphoreTake(lora_semaphore, 100);
				nodes[ID].node_id = ID;
				nodes[ID].dimmer[0] = byte[0];
				nodes[ID].dimmer[1] = byte[1];
				iotech_lora_send_data_no_ack(&nodes[ID]);
				iotech_lora_wait_to_send();
				xSemaphoreGive(lora_semaphore);
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
				osDelay(1);
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);	
				packet_is_valid = 0;
			}
  }
  /* USER CODE END esp82 */
}

/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)	
{
 if(GPIO_Pin == DIO0_Pin)
 {
	
//	osSemaphoreRelease(RxBinaryHandle);
	 HAL_GPIO_TogglePin(LED4_GPIO_Port ,LED4_Pin);
 }
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
