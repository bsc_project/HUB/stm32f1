#include "iotech_lora.h"

/* Variables */
iotech_sx127x sx = {
LoRa_NSS_GPIO_Port, LoRa_NSS_Pin,
LoRa_RST_GPIO_Port, LoRa_RST_Pin,
&hspi1
};

osSemaphoreId RxBinaryHandlex;


/* Functions */
void iotech_lora_init(uint8_t channel, uint8_t power)
{
	/* begin rfm module */
	iotech_sx127x_begin(&sx, channel, power);
}

void iotech_lora_send_data_no_ack(node_data_t *data)
{
	uint8_t byte[3];
	byte[0] = data->node_id;
	byte[1] = data->dimmer[0];
	byte[2] = data->dimmer[1];
	iotech_sx127x_send(&sx, byte , 3, 1);
}

void iotech_lora_wait_to_send(void)
{
	iotech_sx127x_read_wait_to_send(&sx);
}
