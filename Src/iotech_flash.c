#include "iotech_flash.h"

FLASH_EraseInitTypeDef feitd;
uint32_t res;
uint16_t swap[64];

static void iotech_write_flash(uint16_t data_num, uint16_t data)
{
	uint16_t i = 0;
	
	for(i = 0; i < 32 ; i++)
	{
		swap[i] = *(uint32_t *)(base_address + i*2);
	}
	swap[data_num] = data ;
	
	HAL_FLASH_Unlock();
	feitd.NbPages = 1;
	feitd.PageAddress = base_address;
	feitd.TypeErase = FLASH_TYPEERASE_PAGES;
	HAL_FLASHEx_Erase(&feitd, &res);
	
	for(i = 0 ; i < 32 ; i++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)( base_address + i*2), swap[i]);
	}
	HAL_FLASH_Lock();
}

static uint16_t iotech_read_flash(uint16_t data_num)
{
	return *(uint32_t *)(base_address + data_num*2);
}

uint16_t iotech_read_node_address(void)
{
	return iotech_read_flash(0);
}

uint16_t iotech_read_group_address(uint8_t num)
{
	return iotech_read_flash(num + 2);
}

uint16_t iotech_get_groups_size(void)
{
	return iotech_read_flash(1);
}

void iotech_set_groups_size(uint8_t size)
{
	iotech_write_flash(1, size);
}

void iotech_write_node_address(uint16_t addr)
{
	iotech_write_flash(0, addr);
}

void iotech_add_group(uint16_t addr)
{
	uint16_t size;
	size = iotech_read_flash(1);
	iotech_write_flash(2 + size, addr);
	size++;
	iotech_write_flash(1, size);
}

void iotech_remove_group(uint16_t addr)
{
	uint16_t size, pt, i;
	size = iotech_read_flash(1);
	for(i = 0; i < size; i++)
	{
		if(addr == iotech_read_flash(i + 2))
		{
			break;
		}
	}
	if( i <= size)
	{
		pt = i;
		for(i = pt ; i < size; i++)
		{
			iotech_write_flash(i + 2, iotech_read_flash(i + 3));
		}
		size--;
		iotech_write_flash(1, size);
	}
}