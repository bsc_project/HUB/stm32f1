#include "encoder.h"
#include "usart.h"


void encode_send_DMA(uint8_t *data,uint8_t len,UART_HandleTypeDef *uart)
{
	uint8_t i = 0;
	uint8_t check_sum = 0;
	packet[0] = 255;
	packet[1] = 255;
	packet[2] = data[2];
	packet[3] = len;
	packet[4] = data[0];
	packet[5] = data[1];
	check_sum = 255 - (data[0] + data[1] + data[2] +len);
	packet[6] = check_sum;
	HAL_UART_Transmit_DMA(uart,packet,7);
}
