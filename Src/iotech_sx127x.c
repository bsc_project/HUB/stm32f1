#include "iotech_sx127x.h"
#include <string.h>
uint8_t  _implicitHeaderMode ;
uint64_t _frequency;
uint16_t _packetIndex;


static uint8_t iotech_sx127x_writeregister(iotech_sx127x *rfm,uint8_t name,uint8_t value)
{
	uint8_t status;
	uint8_t temp;
	temp = name | iotech_sx127x_SPI_WRITE_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Transmit(rfm->hspi,&value,1,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
static uint8_t iotech_sx127x_writeregisters(iotech_sx127x *rfm,uint8_t name,uint8_t *values,uint8_t width)
{
	uint8_t status;
	uint8_t temp;
	temp = name | iotech_sx127x_SPI_WRITE_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Transmit(rfm->hspi,values,width,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
uint8_t iotech_sx127x_readregister(iotech_sx127x *rfm,uint8_t name)
{
	uint8_t value;
	uint8_t temp;
	temp = name & iotech_sx127x_SPI_READ_MASK;
  HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_Transmit(rfm->hspi,&temp,1,10);
	HAL_SPI_Receive(rfm->hspi,&value,1,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return value;
}

static uint8_t iotech_sx127x_readregisters(iotech_sx127x *rfm,uint8_t name,uint8_t *dest,uint8_t width)
{
	uint8_t status;
	uint8_t temp;
	temp = name & iotech_sx127x_SPI_READ_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Receive(rfm->hspi,dest,width,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
void iotech_sx127x_sleep(iotech_sx127x *rfm)
{
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_01_OP_MODE,iotech_sx127x_MODE_SLEEP|iotech_sx127x_LONG_RANGE_MODE);
}
void iotech_sx127x_idle(iotech_sx127x *rfm)
{
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_01_OP_MODE, iotech_sx127x_MODE_STDBY | iotech_sx127x_LONG_RANGE_MODE);
}
void iotech_sx127x_implicitHeaderMode(iotech_sx127x *rfm)
{
	_implicitHeaderMode = 1;
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1, iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1) | iotech_sx127x_IMPLICIT_HEADER_MODE_ON);
}
void iotech_sx127x_explicitHeaderMode(iotech_sx127x *rfm)
{
	_implicitHeaderMode = 0;
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1, iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1) & iotech_sx127x_IMPLICIT_HEADER_MODE_OFF);
}
void iotech_sx127x_setTxPower(iotech_sx127x *rfm,int level)
{
	if (level < 2) {
    level = 2;
  } else if (level > 17) {
    level = 17;
  }
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0B_OCP,iotech_sx127x_OCP_MAX);
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_4D_PA_DAC,iotech_sx127x_PA_DAC_ENABLE);
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_09_PA_CONFIG, iotech_sx127x_PA_BOOST | (level - 2));
	
}
void iotech_sx127x_setFrequency(iotech_sx127x *rfm,uint64_t frequency)
{
	_frequency = frequency;

  uint64_t frf = ((uint64_t)frequency << 19) / 32000000;
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_06_FRF_MSB, (uint8_t)(frf >> 16));
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_07_FRF_MID, (uint8_t)(frf >> 8));
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_08_FRF_LSB, (uint8_t)(frf >> 0));
}

void iotech_sx127x_setSpreadingFactor(iotech_sx127x *rfm,int sf)
{
	if (sf < 6) {
    sf = 6;
  } else if (sf > 12) {
    sf = 12;
  }

  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2, (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2) & 0x0f) | ((sf << 4) & 0xf0));	
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1F_SYMB_TIMEOUT_LSB, 0x0f);	
}
void iotech_sx127x_setSignalBandwidth(iotech_sx127x *rfm,long sbw)
{
	int bw;
  if (sbw <= 7.8E3) bw = 0;
  else if (sbw <= 10.4E3) bw = 1;
  else if (sbw <= 15.6E3) bw = 2;
  else if (sbw <= 20.8E3) bw = 3;
  else if (sbw <= 31.25E3)bw = 4;
  else if (sbw <= 41.7E3) bw = 5;
  else if (sbw <= 62.5E3) bw = 6;
  else if (sbw <= 125E3)  bw = 7;
  else if (sbw <= 250E3)  bw = 8;
  else bw = 9;
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1, (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1) & 0x0f) | (bw << 4));
}
void iotech_sx127x_setCodingRate4(iotech_sx127x *rfm,int denominator)
{
	if (denominator < 5) {
    denominator = 5;
  } else if (denominator > 8) {
    denominator = 8;
  }

  int cr = denominator - 4;

  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1, (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1D_MODEM_CONFIG1) & 0xf1) | (cr << 1));
}
void iotech_sx127x_setPreambleLength(iotech_sx127x *rfm,long length)
{
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_20_PREAMBLE_MSB, (uint8_t)(length >> 8));
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_21_PREAMBLE_LSB, (uint8_t)(length >> 0));
}
void iotech_sx127x_crc(iotech_sx127x *rfm)
{
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2, iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2) | 0x04);
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1C_HOP_CHANNEL,iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1C_HOP_CHANNEL) | 0x40);
}
void iotech_sx127x_noCrc(iotech_sx127x *rfm)
{
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2, iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1E_MODEM_CONFIG2) & 0xfb);
}
uint8_t iotech_sx127x_begin(iotech_sx127x *rfm,uint8_t channel,uint8_t power)
{
	  // perform reset
  HAL_GPIO_WritePin(rfm->RESET_PORT,rfm->RESET_PIN, GPIO_PIN_RESET);
	HAL_Delay(10);
  HAL_GPIO_WritePin(rfm->RESET_PORT,rfm->RESET_PIN, GPIO_PIN_SET);
	HAL_Delay(10);
  HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN, GPIO_PIN_SET);
	iotech_sx127x_sleep(rfm);
	iotech_sx127x_setFrequency(rfm,(long)(433E6 + channel*1.5E6));
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0E_FIFO_TX_BASE_ADDR,128);
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0F_FIFO_RX_BASE_ADDR,0);
	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0C_LNA,iotech_sx127x_readregister(rfm,iotech_sx127x_REG_0C_LNA) | 0x03 );
//	iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_11_IRQ_FLAGS_MASK,iotech_sx127x_CAD_DETECTED_MASK
//																																	| iotech_sx127x_FHSS_CHANGE_CHANNEL_MASK
//																																	| iotech_sx127x_CAD_DONE_MASK
//																																	| iotech_sx127x_VALID_HEADER_MASK
//																																	| iotech_sx127x_RX_TIMEOUT_MASK);
																										
  iotech_sx127x_crc(rfm);					
	iotech_sx127x_setPreambleLength(rfm,10);
	iotech_sx127x_setSignalBandwidth(rfm,500E3);
  iotech_sx127x_setSpreadingFactor(rfm,6);
	iotech_sx127x_setTxPower(rfm,power);
	iotech_sx127x_idle(rfm);
	
	return 1;
}
void iotech_sx127x_end(iotech_sx127x *rfm)
{
	iotech_sx127x_sleep(rfm);
}
uint8_t iotech_sx127x_beginPacket(iotech_sx127x *rfm,uint8_t implicitHeader)
{
	  // put in standby mode
  iotech_sx127x_idle(rfm);
  if (implicitHeader) iotech_sx127x_implicitHeaderMode(rfm);
   else iotech_sx127x_explicitHeaderMode(rfm);
  
  // reset FIFO address and payload length
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0D_FIFO_ADDR_PTR, 128);
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH, 0);

  return 1;
}
uint8_t iotech_sx127x_endPacket(iotech_sx127x *rfm)
{
	// put in TX mode
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_01_OP_MODE, iotech_sx127x_LONG_RANGE_MODE | iotech_sx127x_MODE_TX);

  // wait for TX done
  while((iotech_sx127x_readregister(rfm,iotech_sx127x_REG_12_IRQ_FLAGS) & iotech_sx127x_TX_DONE) != iotech_sx127x_TX_DONE);
	

  // clear IRQ's
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_12_IRQ_FLAGS, iotech_sx127x_TX_DONE);

  return 1;
}
uint8_t iotech_sx127x_parsePacket(iotech_sx127x *rfm,uint8_t size)
{
	
	uint8_t packetLength = 0;
  uint8_t irqFlags = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_12_IRQ_FLAGS);

  if (size > 0) {
    iotech_sx127x_implicitHeaderMode(rfm);
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH, size & 0xff);
  } else {
    iotech_sx127x_explicitHeaderMode(rfm);
  }

  // clear IRQ's
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_12_IRQ_FLAGS, irqFlags);

  if ((irqFlags & iotech_sx127x_RX_DONE) ) {
    // received a packet
    _packetIndex = 0;

    // read packet length
    if (_implicitHeaderMode) {
      packetLength = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH);
    } else {
      packetLength = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_13_RX_NB_BYTES);
    }

    // set FIFO address to current RX address
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0D_FIFO_ADDR_PTR, iotech_sx127x_readregister(rfm,iotech_sx127x_REG_10_FIFO_RX_CURRENT_ADDR));

    // put in standby mode
    iotech_sx127x_idle(rfm);
  } else if (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_01_OP_MODE) != (iotech_sx127x_LONG_RANGE_MODE | iotech_sx127x_MODE_RXCONTINUOUS)) {
    // not currently in RX mode

    // reset FIFO address
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0D_FIFO_ADDR_PTR, 0);

    // put in single RX mode
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_01_OP_MODE, iotech_sx127x_LONG_RANGE_MODE | iotech_sx127x_MODE_RXCONTINUOUS);
		
  }

  return packetLength;
}
int8_t iotech_sx127x_packetRssi(iotech_sx127x *rfm)
{
	return (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_1A_PKT_RSSI_VALUE) - 137);
}
uint8_t iotech_sx127x_write(iotech_sx127x *rfm,const uint8_t *buffer,uint8_t size)
{
	  uint8_t currentLength = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH);
		
		// check size
  if ((currentLength + size) > MAX_PKT_LENGTH) {
    size = MAX_PKT_LENGTH - currentLength;
  }

  // write data
  for (size_t i = 0; i < size; i++) {
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_00_FIFO, buffer[i]);
  }

  // update length
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH, currentLength + size);

  return size;
}
void iotech_sx127x_receive(iotech_sx127x *rfm,uint8_t size)
{
	
  if (size > 0) {
    iotech_sx127x_implicitHeaderMode(rfm);
    iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_22_PAYLOAD_LENGTH, size & 0xff);
  } else {
    iotech_sx127x_explicitHeaderMode(rfm);
  }
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_01_OP_MODE, iotech_sx127x_LONG_RANGE_MODE | iotech_sx127x_MODE_RXCONTINUOUS);
}

uint8_t iotech_sx127x_available(iotech_sx127x *rfm)
{
	return (iotech_sx127x_readregister(rfm,iotech_sx127x_REG_13_RX_NB_BYTES) - _packetIndex);
}
uint8_t iotech_sx127x_read(iotech_sx127x *rfm)
{
	if (!iotech_sx127x_available(rfm)) {
    return 0;
  }

  _packetIndex++;
  return iotech_sx127x_readregister(rfm,iotech_sx127x_REG_00_FIFO);
}
uint8_t iotech_sx127x_peek(iotech_sx127x *rfm)
{
	  if (!iotech_sx127x_available(rfm)) {
    return 0;
  }


  // store current FIFO address
  int currentAddress = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_0D_FIFO_ADDR_PTR);

  // read
  uint8_t b = iotech_sx127x_readregister(rfm,iotech_sx127x_REG_00_FIFO);

  // restore FIFO address
  iotech_sx127x_writeregister(rfm,iotech_sx127x_REG_0D_FIFO_ADDR_PTR, currentAddress);

  return b;
}
void iotech_sx127x_flush(iotech_sx127x *rfm)
{
	
}
void iotech_sx127x_send(iotech_sx127x *rfm,uint8_t * data,uint8_t size, uint8_t retry)
{
	uint8_t j=0;
				for(j = 0 ; j<retry;j++)
			{
				iotech_sx127x_beginPacket(rfm,1);
				iotech_sx127x_write(rfm,data,size);
				iotech_sx127x_endPacket(rfm);
			}
			
}

void iotech_sx127x_read_wait_to_send(iotech_sx127x *rfm)
{ 
	while((iotech_sx127x_readregister(rfm, iotech_sx127x_REG_12_IRQ_FLAGS) & 1 << 3));
	iotech_sx127x_writeregister(rfm, iotech_sx127x_REG_12_IRQ_FLAGS, 1 << 3);
}
